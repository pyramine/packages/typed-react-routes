/**
 * @public
 */
export type DeepReadonly<T> = T extends (infer R)[]
    ? DeepReadonlyArray<R>
    : // eslint-disable-next-line @typescript-eslint/ban-types
      T extends Function
      ? T
      : // eslint-disable-next-line @typescript-eslint/ban-types
        T extends object
        ? DeepReadonlyObject<T>
        : T;

/**
 * @public
 */
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface DeepReadonlyArray<T> extends ReadonlyArray<DeepReadonly<T>> {}

/**
 * @public
 */
export type DeepReadonlyObject<T> = {
    readonly [P in keyof T]: DeepReadonly<T[P]>;
};

/**
 * This type enhances the type used by react-router to not allow undefined
 * @public
 */
export type RequiredParams<Key extends string = string> = {
    readonly [key in Key]: string;
};
