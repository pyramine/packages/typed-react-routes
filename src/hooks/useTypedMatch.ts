import { PathMatch, useMatch } from 'react-router';
import { RouteConfig } from '../router';
import type { ParamParseKey } from 'react-router';

/**
 * check whether the given route matches the current location
 * @public
 * @param route - route to check for match
 * @see https://reactrouter.com/docs/en/v6/api#usematch
 */
export function useTypedMatch<Path extends string>(
    route: RouteConfig<Path>,
): PathMatch<ParamParseKey<Path>> | null {
    return useMatch({
        path: route.path,
        caseSensitive: route.options.sensitive,
        end: true,
    }) as PathMatch<ParamParseKey<Path>> | null;
}
