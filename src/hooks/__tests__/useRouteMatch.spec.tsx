import { useTypedMatch, defineRoutes, TypedRoutes } from '../../index';
import { act, render } from '@testing-library/react';
import { unstable_HistoryRouter as HistoryRouter } from 'react-router-dom';
import { MemoryRouter } from 'react-router';
import { createTestMemoryHistory } from '../../__tests__/__utils__/CreateTestMemoryHistory';

const routes = defineRoutes((g) => ({
    route1: g.path('/route1').view(Route1),
    route2: g.path('/route2').view(Route2),
}));

function Route1() {
    return (
        <div>
            <Component />
            Route1
        </div>
    );
}

function Route2() {
    return (
        <div>
            <Component />
            Route2
        </div>
    );
}

function Component() {
    const matchesRoute1 = useTypedMatch(routes.routes.route1);
    const matchesRoute2 = useTypedMatch(routes.routes.route2);

    return (
        <>
            <div>{matchesRoute1 ? 'Match1' : 'Not Match 1'}</div>
            <div>{matchesRoute2 ? 'Match2' : 'Not Match 2'}</div>
        </>
    );
}

it('should return true if the route matches', () => {
    const r = render(
        <MemoryRouter initialEntries={['/route1']}>
            <TypedRoutes routes={routes} />
        </MemoryRouter>,
    );

    expect(r.getByText('Route1')).toBeInTheDocument();
    expect(r.queryByText('Route2')).not.toBeInTheDocument();
    expect(r.getByText('Match1')).toBeInTheDocument();
    expect(r.getByText('Not Match 2')).toBeInTheDocument();
});

it('should return false if the route does not match', () => {
    const r = render(
        <MemoryRouter initialEntries={['/route2']}>
            <TypedRoutes routes={routes} />
        </MemoryRouter>,
    );

    expect(r.queryByText('Route1')).not.toBeInTheDocument();
    expect(r.getByText('Route2')).toBeInTheDocument();
    expect(r.queryByText('Match1')).not.toBeInTheDocument();
    expect(r.queryByText('Not Match 2')).not.toBeInTheDocument();
});

it('should turn to true if the location changes to a matching one', async () => {
    const history = createTestMemoryHistory({ initialEntries: ['/route1'] });

    const r = render(
        <HistoryRouter history={history}>
            <TypedRoutes routes={routes} />
        </HistoryRouter>,
    );

    expect(r.getByText('Route1')).toBeInTheDocument();
    expect(r.queryByText('Route2')).not.toBeInTheDocument();
    expect(r.getByText('Match1')).toBeInTheDocument();
    expect(r.getByText('Not Match 2')).toBeInTheDocument();

    act(() => history.push('/route2'));

    expect(r.queryByText('Route1')).not.toBeInTheDocument();
    expect(r.getByText('Route2')).toBeInTheDocument();
    expect(r.queryByText('Match1')).not.toBeInTheDocument();
    expect(r.queryByText('Not Match 2')).not.toBeInTheDocument();
});

it('should turn to false if the location changes to a not matching one', () => {
    const history = createTestMemoryHistory({ initialEntries: ['/route2'] });

    const r = render(
        <HistoryRouter history={history}>
            <TypedRoutes routes={routes} />
        </HistoryRouter>,
    );

    expect(r.queryByText('Route1')).not.toBeInTheDocument();
    expect(r.getByText('Route2')).toBeInTheDocument();
    expect(r.queryByText('Match1')).not.toBeInTheDocument();
    expect(r.queryByText('Not Match 2')).not.toBeInTheDocument();

    act(() => history.push('/route1'));

    expect(r.getByText('Route1')).toBeInTheDocument();
    expect(r.queryByText('Route2')).not.toBeInTheDocument();
    expect(r.getByText('Match1')).toBeInTheDocument();
    expect(r.getByText('Not Match 2')).toBeInTheDocument();
});
