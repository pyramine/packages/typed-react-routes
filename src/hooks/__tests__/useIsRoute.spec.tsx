import { useIsRoute, defineRoutes, TypedRoutes } from '../../index';
import { ReactElement } from 'react';
import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router';

function Component(): ReactElement | null {
    const isRoute = useIsRoute(myRouter.routes.a);

    return <>{isRoute ? 'true' : 'false'}</>;
}

const myRouter = defineRoutes((root) => ({
    a: root.path('/a').view(Component),
    b: root.path('/b').view(Component),
}));

it('should return true if is route', () => {
    const { container } = render(
        <MemoryRouter initialEntries={['/a']}>
            <TypedRoutes routes={myRouter} />
        </MemoryRouter>,
    );

    expect(container).toMatchInlineSnapshot(`
        <div>
          true
        </div>
    `);
});

it('should return false if is not route', () => {
    const { container } = render(
        <MemoryRouter initialEntries={['/b']}>
            <TypedRoutes routes={myRouter} />
        </MemoryRouter>,
    );

    expect(container).toMatchInlineSnapshot(`
        <div>
          false
        </div>
    `);
});
