import { ReactElement, useCallback } from 'react';
import { act, render } from '@testing-library/react';
import { defineRoutes, TypedRoutes, useTypedNavigate } from '../../index';
import { MemoryRouter } from 'react-router';

it('should navigate to provided route', () => {
    function ComponentA(): ReactElement | null {
        const navigate = useTypedNavigate();
        const handleClick = useCallback(() => {
            navigate(routesConfig.routes.b.route({}));
        }, [navigate]);

        return <button onClick={handleClick}>Click Me!</button>;
    }

    function ComponentB() {
        return <>Component B</>;
    }

    const routesConfig = defineRoutes((root) => ({
        a: root.path('/a').view(ComponentA),
        b: root.path('/b').view(ComponentB),
    }));

    const { getByText, queryByText } = render(
        <MemoryRouter initialEntries={['/a']}>
            <TypedRoutes routes={routesConfig} />
        </MemoryRouter>,
    );

    expect(getByText('Click Me!')).toBeInTheDocument();
    expect(queryByText('Component B')).not.toBeInTheDocument();

    act(() => getByText('Click Me!').click());

    expect(queryByText('Click Me!')).not.toBeInTheDocument();
    expect(queryByText('Component B')).toBeInTheDocument();
});
