import { useTypedRoutesConfig } from '../useTypedRoutesConfig';
import { defineRoutes } from '../../utils/defineRoutes';
import { render } from '@testing-library/react';
import { TypedRoutes } from '../../components';
import { MemoryRouter } from 'react-router';

function DummyComponent() {
    const routes = useTypedRoutesConfig();

    expect(routes).toMatchSnapshot();

    return <>Hello World!</>;
}

it('should provide routes through context', () => {
    expect.assertions(1);
    const routes = defineRoutes((p) => ({
        a: p.index().view(DummyComponent),
    }));

    render(
        <MemoryRouter>
            <TypedRoutes routes={routes} />
        </MemoryRouter>,
    );
});

it('should fail if not rendered below TypedRouter', () => {
    expect.assertions(1);

    function Dummy() {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        expect(() => useTypedRoutesConfig()).toThrow(
            'Invariant failed: The useTypedRoutesConfig hook can only be used within a <TypedRouter.XXX />',
        );

        return <></>;
    }

    render(<Dummy />);
});
