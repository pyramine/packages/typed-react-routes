import { TypedRoutesConfig } from '../router';
import { useContext } from 'react';
import { TypedRoutesConfigContext } from '../components/TypedRoutesConfigContext';
import invariant from 'tiny-invariant';

/**
 * access the {@link TypedRoutesConfig} of the nearest provider
 * @public
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function useTypedRoutesConfig(): TypedRoutesConfig<any, any> {
    const value = useContext(TypedRoutesConfigContext);
    invariant(
        value,
        'The useTypedRoutesConfig hook can only be used within a <TypedRouter.XXX />',
    );

    return value;
}
