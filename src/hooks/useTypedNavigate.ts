import { NavigateOptions, useNavigate } from 'react-router';
import { RouteTarget } from '../router';

/**
 * @public
 * The result of the {@link useTypedNavigate} hook
 */
export interface UseTypedNavigateFunction {
    /**
     * this function can be used to navigate to a given {@link RouteTarget}
     * @param to - the target to navigate to
     * @param options - the options
     */ <Path extends string>(
        to: RouteTarget<Path>,
        options?: NavigateOptions,
    ): void;
}

/**
 * Returns a method for changing the location. You can pass a RouteConfig to the returned function to specify the
 * destination.
 * @public
 */
export function useTypedNavigate(): UseTypedNavigateFunction {
    const navigate = useNavigate();

    return (to, options) => {
        navigate(to.path, options);
    };
}
