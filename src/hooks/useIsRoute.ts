import { RouteConfig } from '../router';
import { useTypedMatch } from './useTypedMatch';

/**
 * check whether the current route matches the provided route
 * @param route - the route to check whether it is the current one
 * @public
 */
export function useIsRoute<Path extends string>(
    route: RouteConfig<Path>,
): boolean {
    return !!useTypedMatch(route);
}
