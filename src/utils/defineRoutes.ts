import { RouteObj, GroupHelper, TypedRoutesConfig } from '../router';

/**
 * create a group for root (not path)
 * @param rootRoutes - this callback should return all routes within this group
 * @public
 */
export function defineRoutes<Sub extends RouteObj>(
    rootRoutes: (root: GroupHelper<''>) => Sub,
): TypedRoutesConfig<'', Sub>;
/**
 * creates a group with a basepath for all containing routes
 * @param basePath - the basePath which all sub routes should use
 * @param routes - this callback should return all routes within this group
 * @public
 */
export function defineRoutes<BasePath extends string, Sub extends RouteObj>(
    basePath: BasePath,
    routes: (root: GroupHelper<BasePath>) => Sub,
): TypedRoutesConfig<BasePath, Sub>;
export function defineRoutes<BasePath extends string, Sub extends RouteObj>(
    pathOrRoutes: BasePath | ((root: GroupHelper<''>) => Sub),
    routes?: (root: GroupHelper<BasePath>) => Sub,
): TypedRoutesConfig<BasePath, Sub> | TypedRoutesConfig<'', Sub> {
    if (routes && typeof pathOrRoutes === 'string') {
        return new TypedRoutesConfig<BasePath, Sub>({
            basePath: pathOrRoutes,
            routes: routes,
        });
    }

    if (typeof pathOrRoutes === 'function') {
        return new TypedRoutesConfig<'', Sub>({
            basePath: '',
            routes: pathOrRoutes,
        });
    }

    /* istanbul ignore next */
    throw new Error('this is impossible to reach..');
}
