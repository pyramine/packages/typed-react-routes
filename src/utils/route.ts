import { RouteConfig, RouteTarget, RouteTargetParams } from '../router';

/**
 * Generate a {@link RouteTarget} for a given {@link RouteConfig} with the given parameter
 * @param to - the route configuration to generate the target for
 * @param params - the params to replace in the configurations path
 * @public
 */
export function route<Path extends string>(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    to: RouteConfig<Path, any>,
    params: RouteTargetParams<Path>,
): RouteTarget<Path> {
    return to.route(params);
}
