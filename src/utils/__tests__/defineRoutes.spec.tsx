import { RouteConfig } from '../../index';
import { defineRoutes } from '../defineRoutes';

it('should return an object with keys', () => {
    const myRouter = defineRoutes((root) => {
        return {
            first: root.path('/first').view(() => <></>),
            second: root.path('/second/:second').view(() => <></>),
        };
    });

    expect(myRouter.routes).toHaveProperty('first');
    expect(myRouter.routes.first).toBeInstanceOf(RouteConfig);
    expect(myRouter.routes.first.path).toBe('/first');
    expect(myRouter.routes).toHaveProperty('second');
    expect(myRouter.routes.second).toBeInstanceOf(RouteConfig);
    expect(myRouter.routes.second.path).toBe('/second/:second');
});

it('should return an object with keys with base', () => {
    const myRouter = defineRoutes('/base', (root) => {
        return {
            first: root.path('/first').view(() => <></>),
            second: root.path('/second/:second').view(() => <></>),
        };
    });

    expect(myRouter.routes).toHaveProperty('first');
    expect(myRouter.routes.first).toBeInstanceOf(RouteConfig);
    expect(myRouter.routes.first.path).toBe('/base/first');
    expect(myRouter.routes).toHaveProperty('second');
    expect(myRouter.routes.second).toBeInstanceOf(RouteConfig);
    expect(myRouter.routes.second.path).toBe('/base/second/:second');
});
