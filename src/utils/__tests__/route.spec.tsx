import { RouteConfigBuilder, RouteTarget } from '../../router';
import { route } from '../route';

it('should create RouteTarget from config and params', () => {
    const config = new RouteConfigBuilder('/:param').view(() => <></>);

    const target = route(config, { param: 'my-param' });
    expect(target).toBeInstanceOf(RouteTarget);
    expect(target.route).toBe(config);
    expect(target.params).toStrictEqual({ param: 'my-param' });
});
