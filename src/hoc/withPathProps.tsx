import { ComponentType } from 'react';
import { useParams } from 'react-router';
import type { ParamParseKey } from 'react-router';
import { RequiredParams } from '../types';

/**
 * @internal
 */
export interface WithPathProps<Path extends string> {
    pathProps: RequiredParams<ParamParseKey<Path>>;
}

/**
 * @internal
 */
export type ComponentWithPathProps<
    Path extends string,
    Props extends WithPathProps<Path>,
> = ComponentType<Omit<Props, keyof WithPathProps<Path>>>;

/**
 * Provide the params of the path to the provided component.
 * This method DOES NOT assert that the component is rendered on the correct path. This needs to be handled
 * elsewhere. It just passes the parameters of the current path to the provided component.
 * @param WrappedComponent - the component which should get the parameters passed.
 * @internal
 */
export function withPathProps<
    Path extends string,
    Props extends WithPathProps<Path>,
>(WrappedComponent: ComponentType<Props>): ComponentWithPathProps<Path, Props> {
    // Try to create a nice displayName for React Dev Tools.
    const displayName =
        WrappedComponent.displayName || WrappedComponent.name || 'Component';

    const ComponentWithPathProps: ComponentWithPathProps<Path, Props> = (
        props,
    ) => {
        const params = useParams<RequiredParams<ParamParseKey<Path>>>();
        return <WrappedComponent {...(props as Props)} pathProps={params} />;
    };

    ComponentWithPathProps.displayName = `withPathProps(${displayName})`;

    return ComponentWithPathProps;
}
