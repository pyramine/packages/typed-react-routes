import { TypedRoutesConfig } from '../router';
import { createContext } from 'react';

/**
 * @internal
 */
export const TypedRoutesConfigContext = createContext<TypedRoutesConfig<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    any,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    any
> | null>(null);
