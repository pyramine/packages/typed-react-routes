import { Link, LinkProps } from 'react-router-dom';
import React, { ReactElement } from 'react';
import { RouteTarget } from '../router';

/**
 * @public
 */
export interface TypedLinkProps<Path extends string>
    extends Omit<LinkProps, 'to'> {
    /**
     * the target this should link to
     */
    to: RouteTarget<Path>;
}

/**
 * Provides declarative, accessible navigation around your application.
 *
 * @public
 * @param props - component props
 * @constructor
 * @see https://reactrouter.com/web/api/Link
 * @see {@link RouteConfig.buildPath}
 */
export function TypedLink<Path extends string>(
    props: TypedLinkProps<Path>,
): ReactElement {
    return <Link {...props} to={props.to.path} />;
}
