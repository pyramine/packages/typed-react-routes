import { PropsWithChildren, ReactElement } from 'react';

/**
 * This component is used to avoid assigning invalid props to the Fragment directly.
 * It is just a wrapper to swallow the props.
 * @internal
 * @param props - component props
 * @constructor
 */
export function DefaultLayout(
    props: PropsWithChildren<unknown>,
): ReactElement | null {
    return <>{props.children}</>;
}
