import React, { ReactElement } from 'react';
import { NavLink, NavLinkProps } from 'react-router-dom';
import { RouteTarget } from '../router';

/**
 * @public
 */
export interface TypedNavLinkProps<Path extends string>
    extends Omit<NavLinkProps, 'to'> {
    /**
     * the target this should link to
     */
    to: RouteTarget<Path>;
}

/**
 * A special version of the <Link> that will add styling attributes to the rendered element when it matches the current URL.
 *
 * @public
 * @param props - component props
 * @constructor
 * @see https://reactrouter.com/web/api/NavLink
 * @see {@link RouteConfig.buildPath}
 */
export function TypedNavLink<Path extends string>(
    props: TypedNavLinkProps<Path>,
): ReactElement {
    return <NavLink {...props} to={props.to.path} />;
}
