import { useRoutes } from 'react-router';
import { TypedRoutesConfig } from '../router';
import { TypedRoutesConfigContext } from './TypedRoutesConfigContext';
import { Location } from '@remix-run/router';
import { ReactElement } from 'react';

/**
 * @public
 */
export interface TypedRoutesProps {
    /**
     * all the routes specified by this router will be rendered.
     */
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    routes: TypedRoutesConfig<any, any>;

    /**
     * @see {@link useRoutes}
     */
    location?: Partial<Location> | string;
}

/**
 * Renders the provides routes
 *
 * @public
 * @param props - component props
 * @constructor
 * @see https://reactrouter.com/hooks/use-routes
 */
export function TypedRoutes(props: TypedRoutesProps): ReactElement {
    const view = useRoutes(props.routes.toRouteObjectArray());

    return (
        <TypedRoutesConfigContext.Provider value={props.routes}>
            {view}
        </TypedRoutesConfigContext.Provider>
    );
}
