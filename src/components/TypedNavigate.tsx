import React, { ReactElement } from 'react';
import { Navigate } from 'react-router';
import { NavigateProps } from 'react-router-dom';
import { RouteTarget } from '../router';

/**
 * @public
 */
export interface TypedNavigateProps<Path extends string>
    extends Omit<NavigateProps, 'to'> {
    /**
     * the target this should navigate to
     */
    to: RouteTarget<Path>;
}

/**
 * Rendering a <Navigate> will navigate to a new location. The new location will override the current location in the
 * history stack, like server-side redirects (HTTP 3xx) do.
 *
 * @public
 * @param props - component props
 * @constructor
 * @see https://reactrouter.com/web/api/Redirect
 * @see {@link RouteConfig.buildPath}
 */
export function TypedNavigate<Path extends string>(
    props: TypedNavigateProps<Path>,
): ReactElement {
    return <Navigate {...props} to={props.to.path} />;
}
