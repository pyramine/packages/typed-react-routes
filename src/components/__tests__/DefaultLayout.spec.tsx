import { render } from '@testing-library/react';
import { DefaultLayout } from '../DefaultLayout';

it('should match snapshot', () => {
    const { container } = render(<DefaultLayout>Content</DefaultLayout>);

    expect(container).toMatchInlineSnapshot(`
        <div>
          Content
        </div>
    `);
});
