import { act, render } from '@testing-library/react';
import { defineRoutes, TypedLink, TypedRoutes } from '../../index';
import { MemoryRouter } from 'react-router';

function DummyComponent() {
    return <>Hello World!</>;
}

it('should render a link to the route', async () => {
    function DefaultComponent() {
        return (
            <>
                <TypedLink to={routes.routes.route.route({})}>
                    Link To
                </TypedLink>
                <>Default</>
            </>
        );
    }

    const routes = defineRoutes((g) => ({
        home: g.path('/').view(DefaultComponent),
        route: g.path('/route').view(DummyComponent),
    }));

    const r = render(
        <MemoryRouter>
            <TypedRoutes routes={routes} />
        </MemoryRouter>,
    );
    expect(r.queryByText('Default')).toBeInTheDocument();
    expect(r.queryByText('Hello World!')).not.toBeInTheDocument();
    expect(r.getByText('Link To')).toHaveAttribute('href', '/route');
    expect(r.container).toMatchSnapshot();

    act(() => r.getByText('Link To').click());

    expect(r.queryByText('Hello World!')).toBeInTheDocument();
    expect(r.container).toMatchSnapshot();
});

it('should render a parameterized link', () => {
    const routes = defineRoutes((g) => ({
        home: g.path('/').view(Component),
        route: g.path('/routes/:route').view(DummyComponent),
    }));

    function Component() {
        return (
            <TypedLink
                to={routes.routes.route.route({ route: 'my-cool-route' })}>
                Link To
            </TypedLink>
        );
    }

    const r = render(
        <MemoryRouter>
            <TypedRoutes routes={routes} />
        </MemoryRouter>,
    );

    expect(r.container).toMatchSnapshot();
});
