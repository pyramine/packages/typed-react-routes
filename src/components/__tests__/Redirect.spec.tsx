import { render } from '@testing-library/react';
import {
    defineRoutes,
    RedirectRouteConfig,
    RouteConfigBuilder,
    TypedRoutes,
} from '../../index';
import { RequiredParams } from '../../types';
import { MemoryRouter } from 'react-router';

function DummyComponent() {
    return <>Hello World!</>;
}

it('should render a link to the route', async () => {
    const routesConfig = defineRoutes((root) => {
        const route = root.path('/route').view(DummyComponent);

        return {
            route,
            home: root.path('/').redirect(route, { map: () => ({}) }),
        };
    });

    const r = render(
        <MemoryRouter>
            <TypedRoutes routes={routesConfig} />
        </MemoryRouter>,
    );

    expect(r.queryByText('Hello World!')).toBeInTheDocument();
    expect(r.container).toMatchSnapshot();
});

it('should have correct properties', () => {
    const route = new RouteConfigBuilder('/route').view(DummyComponent);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const redirectOptions = { map: (props: RequiredParams<string>) => ({}) };
    const home = new RouteConfigBuilder('/').redirect(route, redirectOptions);

    expect(home).toBeInstanceOf(RedirectRouteConfig);
    expect(home.to).toBe(route);
    expect(home.redirectOptions).toBe(redirectOptions);
});
