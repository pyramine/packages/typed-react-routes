import { act, render } from '@testing-library/react';
import { defineRoutes, TypedNavLink, TypedRoutes } from '../../index';
import { MemoryRouter } from 'react-router';

function DummyComponent() {
    return <>Hello World!</>;
}

it('should render a link to the route', () => {
    const routesConfig = defineRoutes((root) => ({
        route: root.path('/route').view(DummyComponent),
        home: root.index().view(() => {
            return (
                <>
                    <TypedNavLink to={routesConfig.routes.route.route({})}>
                        Link To
                    </TypedNavLink>
                    <div>Default</div>
                </>
            );
        }),
    }));

    const r = render(
        <MemoryRouter>
            <TypedRoutes routes={routesConfig} />
        </MemoryRouter>,
    );
    expect(r.queryByText('Default')).toBeInTheDocument();
    expect(r.queryByText('Hello World!')).not.toBeInTheDocument();
    expect(r.getByText('Link To')).toHaveAttribute('href', '/route');
    expect(r.container).toMatchSnapshot();

    act(() => r.getByText('Link To').click());

    expect(r.queryByText('Hello World!')).toBeInTheDocument();
    expect(r.container).toMatchSnapshot();
});
