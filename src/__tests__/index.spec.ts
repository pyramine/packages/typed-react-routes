import * as index from '../index';

it('should have a consistent api', () => {
    expect(index).toMatchInlineSnapshot(`
        {
          "GroupHelper": [Function],
          "RedirectRouteConfig": [Function],
          "RouteConfig": [Function],
          "RouteConfigBuilder": [Function],
          "RouteGroup": [Function],
          "RouteTarget": [Function],
          "TypedLink": [Function],
          "TypedNavLink": [Function],
          "TypedNavigate": [Function],
          "TypedRoutes": [Function],
          "TypedRoutesConfig": [Function],
          "defineRoutes": [Function],
          "route": [Function],
          "useIsRoute": [Function],
          "useTypedMatch": [Function],
          "useTypedNavigate": [Function],
          "useTypedRoutesConfig": [Function],
        }
    `);
});
