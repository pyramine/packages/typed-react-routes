import { createMemoryHistory, MemoryHistoryOptions } from '@remix-run/router';

/**
 * Set default options according to https://github.com/remix-run/react-router/issues/9351#issuecomment-1258349679
 * @param options - options
 */
export function createTestMemoryHistory(options?: MemoryHistoryOptions) {
    return createMemoryHistory({ v5Compat: true, ...options });
}
