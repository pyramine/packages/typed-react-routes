import { ComponentType } from '../RouteConfigBuilder';
import { defineRoutes } from '../../../utils/defineRoutes';
import { act, render } from '@testing-library/react';
import { TypedRoutes } from '../../../components';
import { unstable_HistoryRouter as HistoryRouter } from 'react-router-dom';
import { createTestMemoryHistory } from '../../../__tests__/__utils__/CreateTestMemoryHistory';
import { PropsWithChildren } from 'react';
import { MemoryRouter } from 'react-router';

function DummyComponent() {
    return <>Hello World! </>;
}

function DefaultComponent() {
    return <>Default </>;
}

it('should use hocs for route', async () => {
    let bla = '';

    function withLog<P extends Record<string, unknown>>(
        C: ComponentType<P>,
    ): ComponentType<P> {
        function WithLog(p: P) {
            bla = C.displayName || C.name;
            return <C {...p} />;
        }

        return WithLog;
    }

    const switch1 = defineRoutes((p) => {
        const a = p.path('/a').withHOC('log', withLog).view(DummyComponent);
        const b = p.path('/b').view(DefaultComponent);

        return { a, b };
    });

    const history = createTestMemoryHistory({ initialEntries: ['/b'] });
    expect(bla).toBe('');

    const r = render(
        <HistoryRouter history={history}>
            <TypedRoutes routes={switch1} />
        </HistoryRouter>,
    );
    expect(r.queryByText('Hello World!')).not.toBeInTheDocument();
    expect(r.queryByText('Default')).toBeInTheDocument();
    expect(r.container).toMatchSnapshot();

    act(() => history.push('/a'));
    expect(r.queryByText('Hello World!')).toBeInTheDocument();
    expect(r.queryByText('Default')).not.toBeInTheDocument();
    expect(bla).toBe('DummyComponent');
    expect(r.container).toMatchSnapshot();
});

it('should remove hocs', async () => {
    let bla = '';

    function withLog<P extends Record<string, unknown>>(
        C: ComponentType<P>,
    ): ComponentType<P> {
        function WithLog(p: P) {
            bla = C.displayName || C.name;
            return <C {...p} />;
        }

        return WithLog;
    }

    const switch1 = defineRoutes((p) => {
        const a = p
            .path('/a')
            .withHOC('log', withLog)
            .withoutHOC('log')
            .view(DummyComponent);
        const b = p.path('/b').view(DefaultComponent);

        return { a, b };
    });

    const history = createTestMemoryHistory({ initialEntries: ['/b'] });
    expect(bla).toBe('');

    const r = render(
        <HistoryRouter history={history}>
            <TypedRoutes routes={switch1} />
        </HistoryRouter>,
    );
    expect(r.queryByText('Hello World!')).not.toBeInTheDocument();
    expect(r.queryByText('Default')).toBeInTheDocument();
    expect(r.container).toMatchSnapshot();

    act(() => history.push('/a'));
    expect(r.queryByText('Hello World!')).toBeInTheDocument();
    expect(r.queryByText('Default')).not.toBeInTheDocument();
    expect(bla).toBe('');
    expect(r.container).toMatchSnapshot();
});

it('should warn if removed hoc does not exist', () => {
    const originalWarn = console.warn;
    console.warn = jest.fn();
    defineRoutes((p) => {
        const a = p.path('/a').withoutHOC('log').view(DummyComponent);
        const b = p.path('/b').view(DefaultComponent);

        return { a, b };
    }).routes;
    expect(console.warn).toHaveBeenCalledWith(
        'You are trying to remove a hoc with key: log which does not exist.',
    );
    console.warn = originalWarn;
});

it('should return routes with layout', () => {
    function Layout(props: PropsWithChildren<Record<string, unknown>>) {
        return <div title="Layout">{props.children}</div>;
    }

    function AComponent() {
        return <>Hello World!</>;
    }

    function BComponent() {
        return <>Hello Dumbledore!</>;
    }

    const routes = defineRoutes((p) => ({
        a: p.path('/a').view(AComponent),
        b: p.path('/b').withLayout(Layout).view(BComponent),
    }));

    const ui = render(
        <MemoryRouter initialEntries={['/b']}>
            <TypedRoutes routes={routes} />
        </MemoryRouter>,
    );

    expect(ui.container).toMatchSnapshot();
});

it('should return routes with extended layout', () => {
    function BaseLayout(props: PropsWithChildren<Record<string, unknown>>) {
        return <div title="BaseLayout">{props.children}</div>;
    }

    function Layout(props: PropsWithChildren<Record<string, unknown>>) {
        return <div title="Layout">{props.children}</div>;
    }

    function Component() {
        return <>Hello Dumbledore!</>;
    }

    const routes = defineRoutes((p) => ({
        group: p
            .index()
            .withLayout(BaseLayout)
            .group((n) => ({
                b: n.path('/a').withLayout(Layout, true).view(Component),
            })),
    }));

    const ui = render(
        <MemoryRouter initialEntries={['/a']}>
            <TypedRoutes routes={routes} />
        </MemoryRouter>,
    );

    expect(ui.container).toMatchSnapshot();
});
