import { act, render, waitFor } from '@testing-library/react';
import {
    RouteConfig,
    RouteConfigBuilder,
    defineRoutes,
    TypedRoutes,
} from '../../../index';
import { unstable_HistoryRouter as HistoryRouter } from 'react-router-dom';
import { createTestMemoryHistory } from '../../../__tests__/__utils__/CreateTestMemoryHistory';

function DummyComponent() {
    return <>Hello World!</>;
}

function DefaultComponent() {
    return <>Default</>;
}

const home = new RouteConfigBuilder('/').view(DefaultComponent);

it('should render component with `render`', async () => {
    const routes = defineRoutes((g) => ({
        home,
        route: g.path('/route').view(DummyComponent),
    }));

    const history = createTestMemoryHistory();

    const r = render(
        <HistoryRouter history={history}>
            <TypedRoutes routes={routes} />
        </HistoryRouter>,
    );
    expect(r.queryByText('Hello World!')).not.toBeInTheDocument();
    expect(r.container).toMatchSnapshot();

    act(() => history.push('/route'));
    await waitFor(() =>
        expect(r.queryByText('Hello World!')).toBeInTheDocument(),
    );
    expect(r.container).toMatchSnapshot();
});

it('should render strict withSensitive', async () => {
    const routes = defineRoutes((g) => ({
        home,
        default: g.fallback().view(() => <></>),
        route: g.path('/route').withSensitive().view(DummyComponent),
    }));

    const history = createTestMemoryHistory();
    const r = render(
        <HistoryRouter history={history}>
            <TypedRoutes routes={routes} />
        </HistoryRouter>,
    );
    expect(r.queryByText('Hello World!')).not.toBeInTheDocument();
    expect(r.container).toMatchSnapshot();

    act(() => history.push('/Route'));
    expect(r.queryByText('Hello World!')).not.toBeInTheDocument();
    expect(r.container).toMatchSnapshot();

    act(() => history.push('/route'));
    expect(r.queryByText('Hello World!')).toBeInTheDocument();
    expect(r.container).toMatchSnapshot();
});

it('should have same path as provided', () => {
    const path = '/my-custom-path';
    const route = new RouteConfigBuilder(path).view(DummyComponent);
    expect(route.path).toBe(path);
});

it('should build path correct', () => {
    const route = new RouteConfigBuilder('/group/:group/user/:user').view(
        DummyComponent,
    );
    expect(route.buildPath({ group: 'group-1', user: 'user-3' })).toBe(
        '/group/group-1/user/user-3',
    );
});

it('should redirect on redirect', async () => {
    const routes = defineRoutes((g) => {
        const redirectTo = g.path('/redirected').view(DummyComponent);
        return {
            home,
            route: g.path('/route').redirect(redirectTo, { map: (p) => p }),
            redirectTo,
        };
    });
    const history = createTestMemoryHistory();

    const r = render(
        <HistoryRouter history={history}>
            <TypedRoutes routes={routes} />
        </HistoryRouter>,
    );

    expect(r.queryByText('Hello World!')).not.toBeInTheDocument();
    expect(r.container).toMatchSnapshot();

    act(() => history.push('/route'));
    await waitFor(() =>
        expect(r.queryByText('Hello World!')).toBeInTheDocument(),
    );
    expect(r.container).toMatchSnapshot();
});

it('should get route params', async () => {
    function MyRoute(props: { pathProps: { group?: string; user?: string } }) {
        return (
            <>
                Group:{props.pathProps.group},User:{props.pathProps.user}
            </>
        );
    }

    const routes = defineRoutes((g) => ({
        home,
        route: g.path('/group/:group/user/:user').view(MyRoute),
    }));
    const history = createTestMemoryHistory();

    const r = render(
        <HistoryRouter history={history}>
            <TypedRoutes routes={routes} />
        </HistoryRouter>,
    );
    expect(r.queryByText('Default')).toBeInTheDocument();
    expect(r.container).toMatchSnapshot();

    act(() =>
        history.push(
            routes.routes.route.buildPath({
                group: 'MyGroup',
                user: 'MyUser',
            }),
        ),
    );

    await waitFor(() =>
        expect(r.queryByText('Default')).not.toBeInTheDocument(),
    );
    expect(r.queryByText('Group:MyGroup,User:MyUser')).toBeInTheDocument();
    expect(r.container).toMatchSnapshot();
});

describe('matchPath', function () {
    it('should match path', () => {
        const config = new RouteConfig('/bla', () => null);
        expect(config.matchPath('/bla')).toMatchSnapshot();
    });

    it('should match path with params', () => {
        const config = new RouteConfig('/bla/:param', () => null);
        expect(config.matchPath('/bla/value')).toMatchSnapshot();
    });

    it('should provide null if no match', () => {
        const config = new RouteConfig('/bla/:param', () => null);
        expect(config.matchPath('/nope')).toBeNull();
    });
});

describe('get component', () => {
    it('should provide the built component', () => {
        const component = () => null;
        const config = new RouteConfig('/bla', component);

        expect(config.component).toMatchInlineSnapshot(`
            <withPathProps(DefaultLayout)>
              <withPathProps(component) />
            </withPathProps(DefaultLayout)>
        `);
    });

    it('should provide the built component with layout', () => {
        const component = () => null;
        const layout = () => null;
        const config = new RouteConfig('/bla', component, layout);

        expect(config.component).toMatchInlineSnapshot(`
            <withPathProps(layout)>
              <withPathProps(component) />
            </withPathProps(layout)>
        `);
    });
});
