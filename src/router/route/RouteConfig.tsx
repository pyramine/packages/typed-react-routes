import { generatePath, matchPath, PathMatch, RouteObject } from 'react-router';
import type { ParamParseKey } from 'react-router';
import React, { PropsWithChildren, ReactNode } from 'react';
import { DeepReadonly, RequiredParams } from '../../types';
import {
    ComponentType,
    RouteHOC,
    TypedRouteComponentProps,
} from './RouteConfigBuilder';
import { withPathProps } from '../../hoc/withPathProps';
import { DefaultLayout } from '../../components/DefaultLayout';
import { RouteTarget, RouteTargetParams } from '../target/RouteTarget';

/**
 * @public
 */
export type RenderProps<Path extends string> = RequiredParams<
    ParamParseKey<Path>
>;

/**
 * @public
 */
export interface RouteOptions {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    hoc: Record<string, RouteHOC<any>>;
    /**
     * @see https://reactrouter.com/web/api/Route/sensitive-bool
     */
    sensitive?: boolean;
}

/**
 * @public
 */
export class RouteConfig<
    Path extends string,
    ExtraProps extends object = Record<string, never>,
> {
    protected readonly _path: Path;
    protected readonly _options: RouteOptions;
    protected readonly _component: ReactNode;

    /**
     * @internal
     * @param path - the path this RouteConfig will listen on
     * @param component - the component to render on route match
     * @param layout - the layout to wrap around the component
     * @param options - all options to use for this route
     */
    constructor(
        path: Path,
        component: ComponentType<
            TypedRouteComponentProps<ParamParseKey<Path>> & ExtraProps
        >,
        layout: ComponentType<
            PropsWithChildren<
                TypedRouteComponentProps<ParamParseKey<Path>> & ExtraProps
            >
        > = DefaultLayout,
        options?: RouteOptions,
    ) {
        this._path = path;
        this._options = options ?? { hoc: {} };
        this._component = this.buildComponents(component, layout);
    }

    public get component(): ReactNode {
        return this._component;
    }

    /**
     * the path of this route
     */
    public get path(): Path {
        return this._path;
    }

    /**
     * build the href-path of this route by providing all necessary parameters
     * @param params - this parameters will replace their placeholder of the path
     */
    public buildPath(params: RequiredParams<ParamParseKey<Path>>): string {
        return generatePath(this.path, params);
    }

    /**
     * Create a new {@link RouteTarget} for the given route parameters
     * @param params - the route parameters to use
     */
    route(params: RouteTargetParams<Path>): RouteTarget<Path> {
        return new RouteTarget(this, params);
    }

    /**
     * get the current config-options of this route
     */
    get options(): DeepReadonly<RouteOptions> {
        return this._options;
    }

    private buildComponents(
        Component: ComponentType<
            TypedRouteComponentProps<ParamParseKey<Path>> & ExtraProps
        >,
        Layout: ComponentType<
            PropsWithChildren<
                TypedRouteComponentProps<ParamParseKey<Path>> & ExtraProps
            >
        >,
    ): ReactNode {
        const PreparedLayout = this.prepareLayout(Layout);
        const PreparedComponent = this.prepareComponent(Component);

        return (
            <PreparedLayout>
                <PreparedComponent />
            </PreparedLayout>
        );
    }

    private prepareComponent(
        Component: ComponentType<
            TypedRouteComponentProps<ParamParseKey<Path>> & ExtraProps
        >,
    ): ComponentType<Record<string, never>> {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        let RouteComponent: ComponentType<any> = Component;
        for (const middleware of Object.values(this._options.hoc)) {
            RouteComponent = middleware(RouteComponent);
        }

        return withPathProps(RouteComponent) as ComponentType<
            Record<string, never>
        >;
    }

    private prepareLayout(
        Layout: ComponentType<
            PropsWithChildren<
                TypedRouteComponentProps<ParamParseKey<Path>> & ExtraProps
            >
        >,
    ): ComponentType<PropsWithChildren<Record<string, unknown>>> {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        let RouteLayout: ComponentType<any> = Layout;
        for (const middleware of Object.values(this._options.hoc)) {
            RouteLayout = middleware(RouteLayout);
        }

        return withPathProps(RouteLayout) as ComponentType<
            PropsWithChildren<Record<string, unknown>>
        >;
    }

    /**
     * @internal
     */
    public toRouteObject(): RouteObject {
        return {
            path: this._path,
            caseSensitive: this._options.sensitive,
            element: this._component,
        };
    }

    public matchPath(path: string): PathMatch<ParamParseKey<Path>> | null {
        return matchPath(
            {
                path: this.path,
                caseSensitive: this._options.sensitive,
            },
            path,
        ) as unknown as PathMatch<ParamParseKey<Path>> | null;
    }
}
