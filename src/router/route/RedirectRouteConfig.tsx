import { RouteConfig, RouteOptions } from './RouteConfig';
import { WithPathProps } from '../../hoc/withPathProps';
import { ReactElement } from 'react';
import { TypedNavigate } from '../../components/TypedNavigate';
import type { ParamParseKey } from 'react-router';
import { RequiredParams } from '../../types';
import { DefaultLayout } from '../../components/DefaultLayout';
import { route } from '../../utils/route';

/**
 * @public
 */
export interface RedirectOptions<
    Path extends string,
    RedirectPath extends string,
> {
    replace?: boolean;
    state?: unknown;
    map: (
        props: RequiredParams<ParamParseKey<Path>>,
    ) => RequiredParams<ParamParseKey<RedirectPath>>;
}

/**
 * @public
 */
export class RedirectRouteConfig<
    Path extends string,
    RedirectPath extends string,
> extends RouteConfig<Path> {
    private readonly _to: RouteConfig<RedirectPath>;

    private readonly _redirectOptions: RedirectOptions<Path, RedirectPath>;

    /**
     *
     * @param path - the path this RouteConfig will listen on
     * @param to - the {@link RouteConfig} to redirect to
     * @param redirectOptions - a mapping between the route-parameters of this route and the route to redirect to and whether or not this should create an entry in the history
     * @param options - all options to use for this route
     * @internal
     */
    constructor(
        path: Path,
        to: RouteConfig<RedirectPath>,
        redirectOptions: RedirectOptions<Path, RedirectPath>,
        options?: RouteOptions,
    ) {
        function RedirectHelper({
            pathProps,
        }: WithPathProps<Path>): ReactElement | null {
            const redirectParams = redirectOptions.map(pathProps);
            return (
                <TypedNavigate
                    to={route(to, redirectParams)}
                    replace={redirectOptions.replace}
                    state={redirectOptions.state}
                />
            );
        }

        super(path, RedirectHelper, DefaultLayout, options);

        this._to = to;
        this._redirectOptions = redirectOptions;
    }

    /**
     * get the config this config will redirect to
     */
    get to(): RouteConfig<RedirectPath> {
        return this._to;
    }

    /**
     * get the redirectOptions save for this config
     */
    get redirectOptions(): RedirectOptions<Path, RedirectPath> {
        return this._redirectOptions;
    }
}
