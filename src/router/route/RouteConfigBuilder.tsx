import { GroupHelper } from '../group/GroupHelper';
import { RouteConfig, RouteOptions } from './RouteConfig';
import {
    ComponentClass,
    PropsWithChildren,
    VoidFunctionComponent,
} from 'react';
import { RouteGroup, RouteObj } from '../group/RouteGroup';
import type { ParamParseKey } from 'react-router';
import { RedirectOptions, RedirectRouteConfig } from './RedirectRouteConfig';
import { DefaultLayout } from '../../components/DefaultLayout';
import { RequiredParams } from '../../types';

/**
 * @public
 */
export type ComponentType<P = Record<string, unknown>> =
    | ComponentClass<P>
    | VoidFunctionComponent<P>;

/**
 * @public
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type RouteHOC<Param> = (c: ComponentType<Param>) => ComponentType<any>;

/**
 * @public
 */
export interface TypedRouteComponentProps<Keys extends string> {
    pathProps: RequiredParams<Keys>;
}

/**
 * @public
 */
export class RouteConfigBuilder<
    Path extends string,
    ExtraProps extends object = Record<string, never>,
> {
    protected readonly _path: Path;
    protected _options: RouteOptions;
    protected _layout: ComponentType<
        PropsWithChildren<
            TypedRouteComponentProps<ParamParseKey<Path>> & ExtraProps
        >
    > = DefaultLayout;

    constructor(
        path: Path,
        layout: ComponentType<
            PropsWithChildren<
                TypedRouteComponentProps<ParamParseKey<Path>> & ExtraProps
            >
        > = DefaultLayout,
        options?: RouteOptions,
    ) {
        this._path = path;
        this._layout = layout;
        this._options = options ?? { hoc: {} };
    }

    /**
     * create a redirect-config so when this route gets rendered we will redirect to the given route
     * @param to - the {@link RouteConfig} to redirect to
     * @param options - a mapping between the route-parameters of this route and the route to redirect to and whether or not this should create an entry in the history
     */
    public redirect<RedirectPath extends string>(
        to: RouteConfig<RedirectPath>,
        options: RedirectOptions<Path, RedirectPath>,
    ): RedirectRouteConfig<Path, RedirectPath> {
        return new RedirectRouteConfig<Path, RedirectPath>(
            this._path,
            to,
            options,
            this._options,
        );
    }

    /**
     * create a view-config so when this route gets rendered we will show the provided component
     * @param Component - the component to show on a route-match
     */
    public view(
        Component: ComponentType<
            TypedRouteComponentProps<ParamParseKey<Path>> & ExtraProps
        >,
    ): RouteConfig<Path, ExtraProps> {
        return new RouteConfig<Path, ExtraProps>(
            this._path,
            Component,
            this._layout,
            this._options,
        );
    }

    /**
     * use a hoc for the route component.
     * @param key - the key use for the hoc
     * @param hoc - the hoc to use
     */
    public withHOC<HOCParams>(
        key: string,
        hoc: RouteHOC<HOCParams>,
    ): RouteConfigBuilder<Path, ExtraProps & HOCParams> {
        this._options.hoc[key] = hoc;
        return this as RouteConfigBuilder<Path, ExtraProps & HOCParams>;
    }

    /**
     * remove a hoc from the route
     * @param key - the key to remove
     */
    public withoutHOC(key: string): RouteConfigBuilder<Path, ExtraProps> {
        if (!(key in this._options.hoc)) {
            console.warn(
                `You are trying to remove a hoc with key: ${key} which does not exist.`,
            );
        }

        delete this._options.hoc[key];
        return this;
    }

    /**
     * creates a new group with several sub-routes
     * @param routes - a callback that should return all routes within this group
     */
    public group<Sub extends RouteObj>(
        routes: (groupPath: GroupHelper<Path, ExtraProps>) => Sub,
    ): Readonly<Sub> & RouteGroup<Sub> {
        const resolvedRoutes = routes(
            new GroupHelper<Path, ExtraProps>(
                this._path,
                this._layout,
                this._options,
            ),
        );
        const routeGroup = new RouteGroup(resolvedRoutes);
        return routeGroup.createProxy();
    }

    /**
     * use the option `sensitive` on the Route
     * @param useSensitive - When true, will match if the path is case sensitive.
     * @see https://reactrouter.com/web/api/Route/sensitive-bool
     */
    public withSensitive(
        useSensitive = true,
    ): RouteConfigBuilder<Path, ExtraProps> {
        this._options.sensitive = useSensitive;
        return this;
    }

    public withLayout(
        Layout: ComponentType<
            PropsWithChildren<
                TypedRouteComponentProps<ParamParseKey<Path>> & ExtraProps
            >
        >,
        extend = false,
    ): RouteConfigBuilder<Path, ExtraProps> {
        if (!extend) {
            this._layout = Layout;

            return this;
        }

        const BaseLayout = this._layout;
        const baseLayoutDisplayName =
            BaseLayout.displayName || BaseLayout.name || 'Component';
        const layoutDisplayName =
            Layout.displayName || Layout.name || 'Component';

        const extendedLayout: ComponentType<
            PropsWithChildren<
                TypedRouteComponentProps<ParamParseKey<Path>> & ExtraProps
            >
        > = (props) => {
            return (
                <BaseLayout {...props}>
                    <Layout {...props}>{props.children}</Layout>
                </BaseLayout>
            );
        };
        extendedLayout.displayName = `extendedLayout(${baseLayoutDisplayName}, ${layoutDisplayName})`;
        this._layout = extendedLayout;

        return this;
    }
}
