import { RouteConfig } from '../route/RouteConfig';
import { RequiredParams } from '../../types';
import type { ParamParseKey } from 'react-router';

/**
 * @public
 */
export type RouteTargetParams<Path extends string> = RequiredParams<
    ParamParseKey<Path>
>;

/**
 * @public
 */
export class RouteTarget<Path extends string> {
    /**
     * get underlying {@link RouteConfig} for this target
     */
    public readonly route: RouteConfig<Path>;

    private _params: RouteTargetParams<Path>;

    /**
     * The URL search parameters for this Target. (The values after the "?").
     * You can use this to manipulate the URL search parameters of this target. (no clone/copy)
     */
    public readonly searchParams = new URLSearchParams();

    private _urlBase: string | URL | undefined;

    private static _urlBase: string | URL | undefined;

    /**
     * @internal
     */
    constructor(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        routeConfig: RouteConfig<Path, any>,
        params: RouteTargetParams<Path>,
    ) {
        this.route = routeConfig;
        this._params = params;
    }

    /**
     * create a new RouteTarget for the used {@link RouteConfig}
     * @param override - override some parameters of the target to clone for the newly created on
     */
    clone(override?: Partial<RouteTargetParams<Path>>) {
        return new RouteTarget(
            this.route,
            Object.assign({}, this._params, override),
        );
    }

    /**
     * modify the current target. It changes the parameters of this target
     * @param override - the parameters to override
     */
    modify(override: Partial<RouteTargetParams<Path>>): RouteTarget<Path> {
        this._params = Object.assign({}, this._params, override);

        return this;
    }

    /**
     * get a copy of the current route parameters. Modifying the return object does not change the parameters of the
     * RouteTarget
     */
    get params(): Readonly<RouteTargetParams<Path>> {
        return Object.assign({}, this._params);
    }

    /**
     * Set an URLSearchParameter for this target
     * @param key - the name of the URL parameter
     * @param value - the value of the URL parameter
     */
    search(key: string, value: string): RouteTarget<Path> {
        this.searchParams.set(key, value);

        return this;
    }

    /**
     * remove a previously added URL search parameter
     * @param key - the name of the parameter to remove
     */
    removeSearch(key: string): RouteTarget<Path> {
        this.searchParams.delete(key);

        return this;
    }

    /**
     * Get a URL instance for the current RouteTarget. You can use
     * {@link RouteTarget.(useUrlBase:static) | RouteTarget.useUrlBase} or
     * {@link RouteTarget.(useUrlBase:instance) | target.useUrlBase} target.useUrlBase to change
     * the domain/base url to be used for the URL instance.
     */
    get url() {
        return new URL(this.path, this.resolveUrlBase());
    }

    /**
     * Get the Href string for the current Target.
     * (Also includes the domain)
     * @see url
     */
    get href() {
        return this.url.href;
    }

    /**
     * Change the domain/base url of ALL RouteTargets
     * @param base - the domain/base url to be used
     */
    static useUrlBase(base: string | URL) {
        this._urlBase = base;
    }

    /**
     * Override the domain/base url ONLY FOR THIS RouteTarget
     * @param base - the domain/base url to be used
     */
    useUrlBase(base: string | URL) {
        this._urlBase = base;

        return this;
    }

    private resolveUrlBase() {
        return (
            this._urlBase ??
            RouteTarget._urlBase ??
            this.resolveDefaultUrlBase()
        );
    }

    private resolveDefaultUrlBase() {
        return typeof window !== 'undefined'
            ? window.location.href
            : 'http://localhost';
    }

    /**
     * Get the path for the current RouteTarget. (Without domain, only path and URL search params)
     */
    get path() {
        return this.buildPath();
    }

    private buildPath() {
        const path = this.route.buildPath(this._params);

        if (this.searchParams.toString() !== '') {
            return `${path}?${this.searchParams.toString()}`;
        }

        return path;
    }

    /**
     * Return the full Url as string
     */
    toString() {
        return this.href;
    }

    /**
     * Return the full Url as string
     */
    toJSON() {
        return this.href;
    }
}
