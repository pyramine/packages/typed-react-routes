import { RouteConfig } from '../../route/RouteConfig';
import { Fragment } from 'react';
import { route } from '../../../utils/route';
import { RouteTarget } from '../RouteTarget';

describe('build()', () => {
    it('should generate string', () => {
        const config = new RouteConfig('/foo/:bar/:noe', Fragment);
        const target = route(config, { bar: 'baf', noe: 'nae' });

        expect(target.path).toBe('/foo/baf/nae');
    });

    it('should generate string with search param', () => {
        const config = new RouteConfig('/foo/:bar/:noe', Fragment);
        const target = route(config, { bar: 'baf', noe: 'nae' }).search(
            'my-search',
            'searching',
        );

        expect(target.path).toBe('/foo/baf/nae?my-search=searching');
    });
});

describe('url()', () => {
    it('should generate string', () => {
        const config = new RouteConfig('/foo/:bar/:noe', Fragment);
        const target = route(config, { bar: 'baf', noe: 'nae' });

        expect(target.url).toMatchInlineSnapshot(
            `"http://localhost/foo/baf/nae"`,
        );
    });

    it('should generate string with search param', () => {
        const config = new RouteConfig('/foo/:bar/:noe', Fragment);
        const target = config
            .route({ bar: 'baf', noe: 'nae' })
            .search('my-search', 'searching');

        expect(target.url).toMatchInlineSnapshot(
            `"http://localhost/foo/baf/nae?my-search=searching"`,
        );
    });

    it('should generate string when set url base statically', () => {
        const config = new RouteConfig('/foo/:bar/:noe', Fragment);
        const target = config
            .route({ bar: 'baf', noe: 'nae' })
            .search('my-search', 'searching');

        RouteTarget.useUrlBase('https://example.com');
        expect(target.url).toMatchInlineSnapshot(
            `"https://example.com/foo/baf/nae?my-search=searching"`,
        );

        expect(
            config.route({ bar: 'baf', noe: 'nae' }).href,
        ).toMatchInlineSnapshot(`"https://example.com/foo/baf/nae"`);

        RouteTarget.useUrlBase('http://localhost'); // reset to default
    });

    it('should generate string when set url base', () => {
        const config = new RouteConfig('/foo/:bar/:noe', Fragment);
        const target = config
            .route({ bar: 'baf', noe: 'nae' })
            .search('my-search', 'searching')
            .useUrlBase('https://example.com');

        expect(target.url).toMatchInlineSnapshot(
            `"https://example.com/foo/baf/nae?my-search=searching"`,
        );

        expect(
            config.route({ bar: 'baf', noe: 'nae' }).href,
        ).toMatchInlineSnapshot(`"http://localhost/foo/baf/nae"`);
    });
});

it('should automatically convert to string for JSON.stringify', () => {
    const config = new RouteConfig('/foo/:bar/:noe', Fragment);
    const target = config.route({ bar: 'baf', noe: 'nae' });

    expect(JSON.stringify({ target })).toMatchInlineSnapshot(
        `"{"target":"http://localhost/foo/baf/nae"}"`,
    );
    expect(target).toMatchInlineSnapshot(`"http://localhost/foo/baf/nae"`);
});

it('should automatically convert to string', () => {
    const config = new RouteConfig('/foo/:bar/:noe', Fragment);
    const target = config.route({ bar: 'baf', noe: 'nae' });

    expect(String(target)).toMatchInlineSnapshot(
        `"http://localhost/foo/baf/nae"`,
    );
});

it('should provide route config', () => {
    const config = new RouteConfig('/foo/:bar/:noe', Fragment);
    const target = config.route({ bar: 'baf', noe: 'nae' });

    expect(target.route).toBe(config);
});

it('should provide copy of params', () => {
    const config = new RouteConfig('/foo/:bar/:noe', Fragment);
    const params = { bar: 'baf', noe: 'nae' };
    const target = config.route(params);

    const param1 = target.params;
    const param2 = target.params;

    expect(params).not.toBe(param1);
    expect(params).toStrictEqual(param1);

    expect(params).not.toBe(param2);
    expect(params).toStrictEqual(param2);

    expect(param1).not.toBe(param2);
    expect(param1).toStrictEqual(param2);
});

it('should provide clone', () => {
    const config = new RouteConfig('/foo/:bar/:noe', Fragment);
    const target = config.route({ bar: 'baf', noe: 'nae' });

    expect(target.clone()).not.toBe(target);
    expect(target.clone()).toStrictEqual(target);
});

it('should modify target', () => {
    const config = new RouteConfig('/foo/:bar/:noe', Fragment);
    const target = config.route({ bar: 'baf', noe: 'nae' });

    const modifiedTarget = target.modify({ bar: 'beanie' });

    expect(modifiedTarget).toBe(target); // same instance
    expect(modifiedTarget.params).toStrictEqual({ bar: 'beanie', noe: 'nae' });
    expect(target.params).toStrictEqual({ bar: 'beanie', noe: 'nae' });
});

describe('removeSearch()', () => {
    it('should remove previously added search param', () => {
        const config = new RouteConfig('/foo/:bar/:noe', Fragment);
        const target = config.route({ bar: 'baf', noe: 'nae' });

        target.search('key', 'value');

        expect(target).toMatchInlineSnapshot(
            `"http://localhost/foo/baf/nae?key=value"`,
        );

        target.removeSearch('key');
        expect(target).toMatchInlineSnapshot(`"http://localhost/foo/baf/nae"`);
    });

    it('should not fail if removing non existent search param', () => {
        const config = new RouteConfig('/foo/:bar/:noe', Fragment);
        const target = config.route({ bar: 'baf', noe: 'nae' });

        target.removeSearch('key');
        expect(target).toMatchInlineSnapshot(`"http://localhost/foo/baf/nae"`);
    });
});
