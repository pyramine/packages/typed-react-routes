import { defineRoutes } from '../../index';

function DefaultComponent() {
    return <>Default</>;
}

const myRouter = defineRoutes((root) => ({
    one: root.path('/one').view(DefaultComponent),
    two: root.path('/two').group((twoGroup) => ({
        three: twoGroup.path('/three').view(DefaultComponent),
    })),
}));

it('should have routes by names', () => {
    expect('routes' in myRouter.routes).toBe(true);
    expect('one' in myRouter.routes).toBe(true);
    expect('two' in myRouter.routes).toBe(true);
    expect('three' in myRouter.routes.two).toBe(true);
});

it('should render all routes of router', () => {
    // TODO
});

describe('matches', () => {
    const myRouter = defineRoutes((root) => ({
        one: root.path('/base/:bla').view(DefaultComponent),
        two: root.path('/base/*').view(DefaultComponent),
    }));

    it('should find matches', () => {
        /*
         * I don't believe that the array should only contain one element...
         * But that's what react-router provides us here.
         */
        const matches = myRouter.findMatchesFor('/base/nl');
        expect(matches).toMatchSnapshot();
    });

    it('should provide null if there are no matches', () => {
        const matches = myRouter.findMatchesFor('/nope');
        expect(matches).toBeNull();
    });

    it('should find the best match', () => {
        const matches = myRouter.findBestMatchFor('/base/nl');
        expect(matches).toMatchSnapshot();
    });

    it('should return null if there is no best match', () => {
        const matches = myRouter.findBestMatchFor('/nope');
        expect(matches).toBeNull();
    });
});

it('should convert to RouteObject[] for react-router', () => {
    const myRouter = defineRoutes((root) => ({
        one: root.path('/one').view(DefaultComponent),
        two: root.path('/two').group((twoGroup) => ({
            index: twoGroup.index().view(DefaultComponent),
            three: twoGroup.path('/three').view(DefaultComponent),
            four: twoGroup.path('/four').group((fourGroup) => ({
                five: fourGroup.path('/five').view(DefaultComponent),
            })),
        })),
    }));

    expect(myRouter.toRouteObjectArray()).toMatchSnapshot();
});
