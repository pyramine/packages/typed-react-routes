import { RouteGroup, RouteObj } from './group/RouteGroup';
import { RouteConfigBuilder } from './route/RouteConfigBuilder';
import { GroupHelper } from './group/GroupHelper';
import { matchRoutes, RouteMatch, RouteObject } from 'react-router';
import { RouteConfig } from './route/RouteConfig';
import { DefaultLayout } from '../components/DefaultLayout';

/**
 * all possible options for the `Router` class.
 * @public
 */
export interface TypedRoutesConfigOptions<
    BasePath extends string,
    Sub extends RouteObj,
> {
    /**
     * a base path or prefix
     */
    basePath: BasePath;

    /**
     * a factory which returns all routes
     * @param root - the root route-group
     */
    routes: (root: GroupHelper<BasePath>) => Sub;
}

/**
 * a router used to define routes.
 * @public
 */
export class TypedRoutesConfig<BasePath extends string, Sub extends RouteObj> {
    private _options: TypedRoutesConfigOptions<BasePath, Sub>;

    private _routesCache: (Readonly<Sub> & RouteGroup<Sub>) | null = null;
    private _flattenRoutesCache: RouteConfig<string>[] | null = null;
    private _flattenRouteObjectsCache: RouteObject[] | null = null;

    constructor(options: TypedRoutesConfigOptions<BasePath, Sub>) {
        this._options = options;
    }

    get routes(): Readonly<Sub> & RouteGroup<Sub> {
        if (!this._routesCache) {
            this._routesCache = this.loadRoutes();
        }

        return this._routesCache;
    }

    private loadRoutes(): Readonly<Sub> & RouteGroup<Sub> {
        const routeBuilder = new RouteConfigBuilder(
            this._options.basePath,
            DefaultLayout,
            {
                hoc: {},
            },
        );
        return routeBuilder.group<Sub>(this._options.routes);
    }

    public findMatchesFor(path: string): RouteMatch[] | null {
        if (!this._flattenRoutesCache) {
            this._flattenRoutesCache = this.routes.flatten();
        }

        if (!this._flattenRouteObjectsCache) {
            this._flattenRouteObjectsCache = this._flattenRoutesCache.map((r) =>
                r.toRouteObject(),
            );
        }

        return matchRoutes(
            this._flattenRouteObjectsCache,
            path,
            this._options.basePath,
        );
    }

    public findBestMatchFor(path: string): RouteMatch | null {
        return this.findMatchesFor(path)?.[0] ?? null;
    }

    /**
     * @internal
     * create the {@link react-router#RouteObject} array for the routers of react-router
     * Do not use this from outside of this library!
     */
    toRouteObjectArray(): RouteObject[] {
        return this.routes.toRouteObjectArray();
    }
}
