import { defineRoutes } from '../../../utils/defineRoutes';
import { createElement, ReactElement } from 'react';

it('should generate view config', () => {
    const myRouter = defineRoutes((root) => ({
        a: root.path('/a').view(() => createElement('div')),
        b: root.path('/b').view(() => createElement('div')),
    }));

    expect(myRouter.routes).toMatchSnapshot();
});

it('should generate redirect config', () => {
    const myRouter = defineRoutes((root) => {
        const b = root.path('/b').view(() => createElement('div'));
        return {
            a: root.path('/a').redirect(b, { map: (p) => p }),
            b,
        };
    });

    expect(myRouter.routes).toMatchSnapshot();
});

it('should generate group config', () => {
    const myRouter = defineRoutes((root) => {
        const b = root.path('/b').view(() => createElement('div'));
        return {
            a: root.path('/a').group((aGroup) => ({
                c: aGroup.path('/c').view(() => createElement('div')),
            })),
            b,
        };
    });

    expect(myRouter.routes).toMatchSnapshot();
});

function MyRoute(props: {
    pathProps: { noob?: string; '*'?: string };
}): ReactElement | null {
    return <>{props.pathProps.noob} </>;
}

it('should keep route param types', () => {
    defineRoutes('/root', (rootGroup) => ({
        bla: rootGroup.path('/bla/:noob').group((blaNoobGroup) => ({
            kla: blaNoobGroup.path('/knat').view(MyRoute),
        })),
    }));
    // this test does not expect/assert anything as this should just verify types (will break if types do not match)
});

it('should build index route', () => {
    const myRouter = defineRoutes('/root', (root) => ({
        bla: root.index().view(MyRoute),
    }));

    expect(myRouter.routes.bla.path).toBe('/root');
});

it('should build fallback route', () => {
    const myRouter = defineRoutes('/root', (root) => ({
        bla: root.fallback().view(MyRoute),
    }));

    expect(myRouter.routes.bla.path).toBe('/root/*');
});
