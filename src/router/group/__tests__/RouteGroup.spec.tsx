import { RouteConfig, RouteGroup, defineRoutes } from '../../../index';

function DefaultComponent() {
    return <>Default</>;
}

function routesFactory() {
    return defineRoutes((root) => ({
        one: root.path('/one').view(DefaultComponent),
        two: root.path('/two').group((twoGroup) => ({
            three: twoGroup.path('/three').view(DefaultComponent),
        })),
    })).routes;
}

it('should be instance of RouteGroup', () => {
    const routes = routesFactory();

    expect(routes).toBeInstanceOf(RouteGroup);
    expect(routes.two).toBeInstanceOf(RouteGroup);
});

it('should be able to delete a property', () => {
    const routes = routesFactory();

    expect(Reflect.deleteProperty(routes, 'one')).toBe(false);
    expect(Reflect.deleteProperty(routes, 'routes')).toBe(false);
});

it('should not be possible to define properties', () => {
    const routes = routesFactory();

    expect(() =>
        Object.defineProperty(routes, 'missing', { value: 24 }),
    ).toThrowError(TypeError);
    expect(() =>
        Object.defineProperty(routes, 'one', { value: 56 }),
    ).toThrowError(TypeError);
    expect(() =>
        Object.defineProperty(routes, 'routes', { value: 89 }),
    ).toThrowError(TypeError);
});

it('should have RouteConfigs accessable', () => {
    const routes = routesFactory();

    expect(routes.one).toBeInstanceOf(RouteConfig);
    expect(routes.two.three).toBeInstanceOf(RouteConfig);
});

it('should return correct Property Descriptors', () => {
    const routes = routesFactory();

    expect(Object.getOwnPropertyDescriptor(routes, 'one')).toStrictEqual(
        Object.getOwnPropertyDescriptor(routes.routes, 'one'),
    );
});

it('should have properties', () => {
    const routes = routesFactory();

    expect('routes' in routes).toBe(true);
    expect('one' in routes).toBe(true);
    expect('two' in routes).toBe(true);
});

it('should be extensible', () => {
    const routes = routesFactory();

    expect(Object.isExtensible(routes)).toBe(true);
    expect(Object.isExtensible(routes.two)).toBe(true);
});

it('should expose all keys', () => {
    const routes = routesFactory();
    const expectedProps = ['one', 'two']; // Note: 'routes' is excluded to be easier for users
    const actualProps: string[] = [];

    for (const propName in routes) {
        actualProps.push(propName);
    }

    expect(actualProps).toStrictEqual(expectedProps);
});

it('should be possible to prevent extension', () => {
    const routes = routesFactory();

    expect(Object.isExtensible(routes)).toBe(true);
    Object.preventExtensions(routes);
    expect(Object.isExtensible(routes)).toBe(false);
});

it('should not be possible to set properties', () => {
    const routes = routesFactory();

    expect(
        // @ts-expect-error `one` is readonly
        () => (routes.one = new RouteConfig<'/one'>('/one', DefaultComponent)),
    ).toThrowError(TypeError);

    expect(
        () =>
            // @ts-expect-error `two` is readonly
            (routes.two = new RouteGroup({
                three: routes.two.three,
            })),
    ).toThrowError(TypeError);
});

it('should not be possible to set the prototype', () => {
    const routes = routesFactory();

    expect(() => Object.setPrototypeOf(routes, RouteConfig)).toThrowError(
        TypeError,
    );

    expect(() => Object.setPrototypeOf(routes.two, RouteConfig)).toThrowError(
        TypeError,
    );
});

it('should provide flattened array of all (even nested) routes', () => {
    expect(routesFactory().flatten()).toMatchSnapshot();
});
