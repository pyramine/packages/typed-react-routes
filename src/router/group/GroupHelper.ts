import {
    ComponentType,
    RouteConfigBuilder,
    TypedRouteComponentProps,
} from '../route/RouteConfigBuilder';
import { RouteOptions } from '../route/RouteConfig';
import { PropsWithChildren } from 'react';
import type { ParamParseKey } from 'react-router';

/**
 * @public
 * this class delivers some convenience methods to create sub routes for a group
 */
export class GroupHelper<
    Path extends string,
    ExtraProps extends object = Record<string, never>,
> {
    private readonly _path: Path;
    private readonly _options: RouteOptions;
    private readonly _layout: ComponentType<
        PropsWithChildren<
            TypedRouteComponentProps<ParamParseKey<Path>> & ExtraProps
        >
    >;

    /**
     * @internal
     * @param path - the path used by the group and used as base for all subRoutes
     * @param layout - the layout to wrap around the component
     * @param options - the options set on the group level. this enables us to pass all options down to subRoutes
     */
    constructor(
        path: Path,
        layout: ComponentType<
            PropsWithChildren<
                TypedRouteComponentProps<ParamParseKey<Path>> & ExtraProps
            >
        >,
        options: RouteOptions,
    ) {
        this._path = path;
        this._layout = layout;
        this._options = options;
    }

    /**
     * create a new {@link RouteConfigBuilder} based on the groups path
     * @param path - the path to concat with the groupPath. eg. <groupPath><path>
     */
    path<SubPath extends string>(
        path: SubPath,
    ): RouteConfigBuilder<`${Path}${SubPath}`, ExtraProps> {
        return new RouteConfigBuilder<`${Path}${SubPath}`, ExtraProps>(
            `${this._path}${path}`,
            this._layout as unknown as ComponentType<
                PropsWithChildren<
                    TypedRouteComponentProps<
                        ParamParseKey<`${Path}${SubPath}`>
                    > &
                        ExtraProps
                >
            >, // TODO
            this._options,
        );
    }

    /**
     * create a new {@link RouteConfigBuilder} for the fallback /*
     */
    fallback(): RouteConfigBuilder<`${Path}/*`, ExtraProps> {
        return this.path('/*');
    }

    /**
     * create a new {@link RouteConfigBuilder} for an index route '/'
     */
    index(): RouteConfigBuilder<`${Path}`, ExtraProps> {
        return this.path('');
    }
}
