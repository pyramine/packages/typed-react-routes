import { RouteConfig } from '../route/RouteConfig';
import { RouteGroupProxyHandler } from './RouteGroupProxyHandler';
import { RouteObject } from 'react-router';

/**
 * @public
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type RouteObj = { [key: string]: RouteConfig<string> | RouteGroup<any> };

/**
 * @public
 */
export class RouteGroup<T extends RouteObj> {
    private readonly _routes: T;

    /**
     *
     * @param routes - all routes within this route group
     * @internal
     */
    constructor(routes: T) {
        this._routes = routes;
    }

    /**
     * @internal
     */
    get routes(): T {
        return this._routes;
    }

    /**
     * @internal
     */
    public flatten(): RouteConfig<string>[] {
        const flattenRoutes: RouteConfig<string>[] = [];

        for (const route of Object.values(this.routes)) {
            if (route instanceof RouteGroup) {
                flattenRoutes.push(...route.flatten());
            } else {
                flattenRoutes.push(route);
            }
        }

        return flattenRoutes;
    }

    /**
     * @internal
     */
    createProxy(): RouteGroup<T> & Readonly<T> {
        return new Proxy(
            this,
            new RouteGroupProxyHandler(),
        ) as unknown as Readonly<T> & RouteGroup<T>;
    }

    /**
     * @internal
     * create the {@link react-router#RouteObject} array for the routers of react-router
     * Do not use this from outside of this library!
     */
    public toRouteObjectArray(): RouteObject[] {
        const routeNodes: RouteObject[] = [];
        for (const routesKey in this.routes) {
            const route = this.routes[routesKey];
            if (route instanceof RouteConfig) {
                routeNodes.push(route.toRouteObject());
            }

            if (route instanceof RouteGroup) {
                const subRoutes = route.toRouteObjectArray();
                routeNodes.push(...subRoutes);
            }
        }

        return routeNodes;
    }
}
