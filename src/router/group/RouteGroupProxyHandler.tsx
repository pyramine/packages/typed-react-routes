import { RouteGroup } from './RouteGroup';

/**
 * @internal
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export class RouteGroupProxyHandler<T extends RouteGroup<any>>
    implements ProxyHandler<T>
{
    defineProperty(): boolean {
        return false;
    }

    deleteProperty(): boolean {
        return false;
    }

    get(target: T, p: string | symbol, receiver: unknown): unknown {
        return (
            Reflect.get(target, p, receiver) ?? Reflect.get(target.routes, p)
        );
    }

    getOwnPropertyDescriptor(
        target: T,
        p: string | symbol,
    ): PropertyDescriptor | undefined {
        return (
            Reflect.getOwnPropertyDescriptor(target, p) ??
            Reflect.getOwnPropertyDescriptor(target.routes, p)
        );
    }

    getPrototypeOf(target: T): object | null {
        return Reflect.getPrototypeOf(target);
    }

    has(target: T, p: string | symbol): boolean {
        return Reflect.has(target, p) || Reflect.has(target.routes, p);
    }

    isExtensible(target: T): boolean {
        return Reflect.isExtensible(target);
    }

    ownKeys(target: T): ArrayLike<string | symbol> {
        return Reflect.ownKeys(target.routes);
    }

    preventExtensions(target: T): boolean {
        return Reflect.preventExtensions(target);
    }

    set(): boolean {
        return false;
    }

    setPrototypeOf(): boolean {
        return false;
    }
}
