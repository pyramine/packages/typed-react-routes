import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import typescript from 'rollup-plugin-typescript2';
import { visualizer } from 'rollup-plugin-visualizer';
import { defineConfig } from 'rollup';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const packageJson = require('./package.json');

export default defineConfig({
    input: 'src/index.ts',
    output: [
        {
            file: packageJson.main,
            format: 'cjs',
            exports: 'named',
            sourcemap: true,
            inlineDynamicImports: true,
        },
        {
            file: packageJson.module,
            format: 'esm',
            exports: 'named',
            sourcemap: true,
            inlineDynamicImports: true,
        },
    ],
    external: /node_modules/, // this works because of having @rollup/plugin-node-resolve
    plugins: [
        resolve(),
        commonjs(),
        typescript({
            useTsconfigDeclarationDir: true,
        }),
        visualizer({
            filename: 'package-deps.html',
            template: 'sunburst',
            gzipSize: true,
            brotliSize: true,
        }),
    ],
});
