/* eslint-disable @typescript-eslint/no-var-requires */
const { execSync } = require('child_process');

const reactVersion = process.env.REACT_VERSION ?? '18';

function install(packageName, dev = true) {
    console.log(execSync(`npm i ${dev ? '-D' : ''} ${packageName}`).toString());
}

function installDepsForReact17() {
    install('@testing-library/react@12 @types/react@17 react@17 react-dom@17');
}

function installDepsForReact18() {
    install('@testing-library/react@13 @types/react@18 react@18 react-dom@18');
}

switch (reactVersion) {
    case '17':
        installDepsForReact17();
        break;
    case '18':
    default:
        installDepsForReact18();
        break;
}
