---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Typed-React-Routes Docs"
  tagline: "Type-safe routing for your React-Router project."
  actions:
    - theme: brand
      text: Get Started
      link: /installation

features:
  - title: Feature A
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: Feature B
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: Feature C
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---

