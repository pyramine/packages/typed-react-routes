import { defineConfig } from 'vitepress';

// https://vitepress.dev/reference/site-config
export default defineConfig({
    base: '/packages/typed-react-routes',
    outDir: '../public',
    title: 'Typed-React-Routes Docs',
    description: 'A VitePress Site',
    themeConfig: {
        // https://vitepress.dev/reference/default-theme-config
        nav: [
            { text: 'Home', link: '/' },
            { text: 'Docs', link: '/docs' },
        ],

        sidebar: [
            {
                text: 'Docs',
                items: [
                    {
                        text: 'Installation',
                        link: '/installation',
                    },
                    {
                        text: 'Defining Routes',
                        items: [
                            { text: 'Basics', link: '/define-routes/basics' },
                            {
                                text: 'Redirects',
                                link: '/define-routes/redirects',
                            },
                            { text: 'Groups', link: '/define-routes/groups' },
                            {
                                text: 'HOCs / Middlewares',
                                link: '/define-routes/hoc',
                            },
                        ],
                    },
                    {
                        text: 'Building Links',
                        link: '/building-links',
                    },
                ],
            },

            { text: 'FAQ', link: '/faq' },
        ],

        socialLinks: [
            {
                icon: 'github',
                link: 'https://gitlab.com/pyramine/packages/typed-react-routes',
            },
        ],
    },
});
