# Defining Routes-Config

For this we will create a file called `src/routes.tsx`. (You can put this file where
ever you want.)

```tsx [routes.tsx]
import {defineRoutes} from '@pyramine/typed-react-routes';
import {Landing} from './pages/Landing.tsx';

export const routesConfig = defineRoutes((root) => ({
    index: root.path('/').view(Landing),
}));

export function routes() {
    return routesConfig.routes;
}
```

Here we create a basic route listening for the `/` path and rendering a simple react component `Landing`.

## Base path

If you need to have a base-path for your entire application you can simply pass it as the first argument
to `defineRoutes`:

```tsx [routes.tsx]
export const routesConfig = defineRoutes('/base-path', (root) => ({
    index: root.path('/').view(Landing),
}));
```

## Case-Sensitivity

If you need your route to be case-sensitive you can just call `withSensitive` on any route before calling `.view`:

```tsx
root.path('/').withSensitive().view(Landing)
root.path('/').withSensitive(false).view(Landing)
```

> If creating a [Group](/define-routes/groups), this option will also apply to ALL sub-routes within this particular
> group.

## Layouts

Just like `<Outlet />`s of `react-router` you can define a "wrapping" component. It acts like a layout for the defined
route or for all sub-routes on groups.

```tsx
root.path('/').withLayout(Layout).view(Landing)
```

This will override any previously specified layouts with the provided one.
But if you want to extend a layout up in the hierarchy, you can pass `true` as the second parameter:

```tsx
root.path('/').withLayout(Layout, true).view(Landing)
```

So a little recap:

```tsx [extend=false]
root.path('/').withLayout(Layout1).group((group) => ({
    index: group.path('/').withLayout(Layout2).view(Landing),
}))

// Result:
// <Layout2>
//   <Landing />
// </Layout2>
```

```tsx [extend=true]
root.path('/').withLayout(Layout1).group((group) => ({
    index: group.path('/').withLayout(Layout2, true).view(Landing),
}))

// Result:
// <Layout1>
//   <Layout2>
//     <Landing />
//   </Layout2
// </Layout1>
```
