# Redirects

Even defining redirects is fully type safe. The reason is you need to pass another route-config as the target of the
redirect.

```tsx [extend=false]
const target = root.path('/target').view(Landing);
const redirect = root.path('/redirect').redirect(target, {
    replace: true, // optional
    state: {myKey: 'myValue'}, // optional
    map: (_) => ({}), // required mapping of route params
});
```

## Route-Params

If the target or the source have route params we need to map the params for the redirect. That's what the `map` option
is for.

### Target only Param

```tsx [extend=false]
const target = root.path('/foo/:bar').view(Landing);
const redirect = root.path('/redirect').redirect(target, {
    map: (_) => ({
        bar: 'my static value',
    }),
});
```

### Source only Param

```tsx [extend=false]
const target = root.path('/target').view(Landing);
const redirect = root.path('/foo/:bar').redirect(target, {
    map: (_) => ({}),
});
```

If the target has no params the mapping is pretty straight forward ;)

### Source & Target params

```tsx [extend=false]
const target = root.path('/foo/:bar').view(Landing);
const redirect = root.path('/foo2/:bar2').redirect(target, {
    map: (sourceParams) => ({
        bar: sourceParams.bar2,
    }),
});
```

Here define an explicit mapping from the source param `bar2` to the target param `bar`.
