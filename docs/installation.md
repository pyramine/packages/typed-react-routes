# Installation

The installation process is fairly easy.

## Install Deps

First of all we need to install the dependencies:

::: code-group

```bash [npm]
npm i @pyramine/typed-react-routes react-router react-router-dom
```

```bash [yarn]
yarn add @pyramine/typed-react-routes react-router react-router-dom
```

```bash [pnpm]
pnpm i @pyramine/typed-react-routes react-router react-router-dom
```

:::

> You better install `react-router` and `react-router-dom` on your own, as these are only `deerDependencies` of this
> package. This is to ensure there is only one version of these packages installed, and YOU are in control of its
> version.

## Your first routes

Next we need to define our routes. For this we will create a file called `src/routes.tsx`. (You can put this file where
ever you want.)

```tsx [routes.tsx]
import { defineRoutes } from '@pyramine/typed-react-routes';
import { Landing } from './pages/Landing.tsx';

export const routesConfig = defineRoutes((root) => ({
    index: root.path('/').view(Landing),
}));

export function routes() {
    return routesConfig.routes;
}
```

## Rendering the routes

```tsx [App.tsx]
import { BrowserRouter } from 'react-router-dom';
import { TypedRoutes } from '@pyramine/typed-react-routes';
import { routesConfig } from './routes.tsx';

function App() {
    return (
        <BrowserRouter>
            <TypedRoutes routes={routesConfig}/>
        </BrowserRouter>
    );
}

export default App;
```

That's it. You should now be able to see your landing page in the browser when hitting `/`.
