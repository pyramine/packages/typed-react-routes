## Why are the new v6.4 Data-Routers not supported?

The handling of "wrapping" contexts is not well-supported by `react-router`. If you are using `@apollo/client` for
example, there is currently no way of wrapping your whole application in its `<ApolloProvider />` component.

I plan to support them soon, but had no time to think of a nice solution yet.

See:

- https://github.com/remix-run/react-router/discussions/10491

## Why am I required to specify the `map` function for Redirects?

That is because I could not figure out how to do it fully type-safe. My vision would be that it can be omitted if there
are no params for the target route, or the source and target params are having the same name. But it is required, if
there needs to be mapping. I could probably just make everything optional, but then typescript wouldn't enforce the
mapping if necessary. Maybe you are a Typescript-wizard, then please have a look and open a Merge-Request :D.

## Why am I required to specify params for Routes without Params?

Same answer as
for: [Why am I required to specify the `map` function for Redirects?](#why-am-i-required-to-specify-the-map-function-for-redirects)
